package 
{
	import core.Game;
	import flash.display.Sprite;
	import flash.events.Event;
	import starling.core.Starling;

	public class Main extends Sprite 
	{
		private var _starling:Starling;
	 
		public function Main():void 
		{
			_starling = new Starling(Game, this.stage);
			_starling.showStats = true;	
			_starling.start();
		}
		
	}
	
}