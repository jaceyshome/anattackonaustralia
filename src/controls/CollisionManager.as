package controls 
{
	import core.Assets;
	import core.Game;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import vos.Bullet;
	import vos.EnemyBullet;
	import states.Play;
	import vos.EnemyNormal;
	/**
	 * ...
	 * @author 
	 */
	public class CollisionManager 
	{
		private var play:Play;
		private var p1:Point = new Point();
		private var p2:Point = new Point();
		private var count:int = 0;
		
		public function CollisionManager(play:Play) 
		{
			this.play = play;
		}
		
		public function update():void 
		{
			enemyBulletAndHero();
			heroBulletAndEnemy();
			count++;
		}
		
		private function heroBulletAndEnemy():void 
		{
			var enemies:Array = play.enemyNormalManager.enemies;
			var bullets:Array = play.bulletManager.bullets;
			var bullet:Bullet;
			var enemy:EnemyNormal;
			
			if (bullets == null || enemies == null) 
			{
				return;
			}
			
			for (var i:int = bullets.length - 1; i >= 0 ; i--) 
			{
				bullet = bullets[i] as Bullet;
				for (var j:int = enemies.length - 1; j >= 0 ; j--) 
				{
					enemy = enemies[j] as EnemyNormal;
					if (hit(enemy, bullet)) 
					{
						play.bulletManager.destroyBullet(bullet);
						enemy.getAttack();
						play.addScore(10);
						if( enemy.getLife() <= 0)
						{
							enemy.resetLife();
							play.explosionManager.spawn(enemy.x, enemy.y);
							Assets.explosion.play();
							play.enemyNormalManager.destroyEnemyNormal(enemy);
						}
					}
					
				}
			}
		}
		
		private function enemyBulletAndHero():void 
		{
			var enemyBullets:Array = play.enemyNormalManager.enemyNormalBulletManager.enemyBullets;
			var enemyBullet:EnemyBullet;
			
			for (var i:int = enemyBullets.length - 1; i >= 0 ; i--) 
			{
				enemyBullet = enemyBullets[i] as EnemyBullet;

				if (hit(play.hero, enemyBullet)) 
				{
					//trace("attack");
					play.enemyNormalManager.enemyNormalBulletManager.destroyEnemyBullet(enemyBullet);
					play.updateHeroLife(play.hero.getAttack());
					if( play.hero.getLife() <= 0)
					{
						play.explosionManager.spawn(play.hero.x, play.hero.y);
						Assets.explosion.play();
						play.game.changeState(Game.GAMEOVER_STATE);
						break;
					}
				}
			}
			
		}
		 
		// Method to check collision
		public function hit(a:Object, b:Object):Boolean
		{
			return a.getBounds(a.root).intersects(b.getBounds(b.root));
		}

			
		/*private function bulletsAndAliens():void 
		{
			var ba:Array = play.bulletManager.bullets;
			var aa:Array = play.EnemyNormalManager.aliens;
			
			var b:Bullet;
			var a:Alien;
			
			for (var i:int = ba.length - 1; i >= 0 ; i--) 
			{
				b = ba[i];
				for (var j:int = aa.length - 1; j >= 0 ; j--)
				{
					a = aa[j];
					//distance collision
					p1.x = b.x;
					p1.y = b.y;
					p2.x = a.x;
					p2.y = a.y;
					if (Point.distance(p1, p2) < a.pivotY + b.pivotY) 
					{
						Assets.explosion.play();
						play.explosionManager.spawn(a.x, a.y);
						play.EnemyNormalManager.destroyEnemyNormal(a);
						play.bulletManager.destroyBullet(b);
						play.score.addScore(200);
					}
				}
			}
		}*/
	}

}