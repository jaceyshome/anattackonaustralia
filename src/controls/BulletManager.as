package controls 
{
	import core.Assets;
	import vos.Bullet;
	import states.Play;
	
	public class BulletManager 
	{
		private var play:Play;
		private var _bullets:Array;
		private var pool:StarlingPool;
		private var count:int;
		
		public function BulletManager(play:Play) 
		{
			this.play = play;
			_bullets = new Array();
			pool = new StarlingPool(Bullet,100);
		}
		
		public function update():void 
		{
			var b:Bullet;
			for (var i:int = _bullets.length - 1; i >= 0; i--) 
			{
				b = _bullets[i];
				b.y -= 25;
				if (b.y < 0) 
				{
					destroyBullet(b);
				}
			}
			
			//trace("play.fire:", play.fire);
			
			if (play.fire && count%10 == 0) 
			{
				fire();
			}
			
			count++;
		}
		
		private function  fire():void 
		{
			var b:Bullet = pool.getSprite() as Bullet;
			play.addChild(b);
			b.x = play.hero.x - 20;
			b.y = play.hero.y - 45;
			_bullets.push(b);
			
			b = pool.getSprite() as Bullet;
			play.addChild(b);
			b.x = play.hero.x + 10;
			b.y = play.hero.y - 45;
			_bullets.push(b);
			
			Assets.shoot.play();
		}
		
		public function resetBulletCounter():void 
		{
			count = 0;
		}
		
		public function destroyBullet(b:Bullet):void 
		{
			var len:int = _bullets.length;
			
			for (var i:int = 0; i < len; i++) 
			{
				if (_bullets[i] == b) 
				{
					_bullets.splice(i, 1);
					b.removeFromParent(true);
					pool.returnSprite(b);
				}
			}
		}
		
		public function desctroy():void
		{
			pool.destroy();
			pool = null;
			_bullets = null;
		}
		
		public function get bullets():Array 
		{
			return _bullets;
		}
		
		public function set bullets(value:Array):void 
		{
			_bullets = value;
		}
	}

}