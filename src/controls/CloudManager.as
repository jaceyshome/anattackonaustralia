package controls 
{
	import core.Assets;
	import core.Enum;
	import vos.Cloud;
	import states.Play;
	
	public class CloudManager 
	{
		private var play:Play;
		private var _clouds:Array;
		private var pool:StarlingPool;
		private var count:int;
		
		public function CloudManager(play:Play) 
		{
			this.play = play;
			_clouds = new Array();
			pool = new StarlingPool(Cloud,Enum.cloudTotalNumber);
		}
		
		public function update():void 
		{
			if (Math.random() < Enum.cloudSpawnWait)
			{
				spawn();	
			}
			
			var cloud:Cloud;
			for (var i:int = _clouds.length - 1; i >= 0; i--) 
			{
				cloud = _clouds[i] ;
				cloud.y += cloud.speed;
				if (cloud.y > Enum.stageHeight) 
				{
					destroyCloud(cloud);
				}
			}
		}
		
		private function spawn():void 
		{
			var cloud:Cloud = pool.getSprite() as Cloud;
			cloud.y = -500;
			cloud.x = Math.random() * 700 + 50;
			play.addChild(cloud);
			_clouds.push(cloud);
		}
		
		public function destroyCloud(cloud:Cloud):void 
		{
			var len:int = _clouds.length;
			
			for (var i:int = 0; i < len; i++) 
			{
				if (_clouds[i] == cloud) 
				{
					_clouds.splice(i, 1);
					cloud.removeFromParent(true);
					pool.returnSprite(cloud);
				}
			}
		}
		
		public function desctroy():void
		{
			pool.destroy();
			pool = null;
			_clouds = null;
		}
		
		public function get clouds():Array 
		{
			return _clouds;
		}
		
		public function set clouds(value:Array):void 
		{
			_clouds = value;
		}
	}

}