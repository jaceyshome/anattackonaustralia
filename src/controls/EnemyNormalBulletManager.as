package controls 
{
	import core.Assets;
	import core.Enum;
	import vos.EnemyBullet;
	import vos.EnemyNormal;
	import states.Play;
	
	public class EnemyNormalBulletManager 
	{
		private var enemy:EnemyNormal;
		private var _enemyBullets:Array;
		private var pool:StarlingPool;
		private var play:Play;
		
		public function EnemyNormalBulletManager(play:Play) 
		{
			this.play = play;
			_enemyBullets = new Array();
			pool = new StarlingPool(EnemyBullet,Enum.enemyNormalBulletMaxNumber);
		}
		
		public function update():void 
		{
			var b:EnemyBullet;
			for (var i:int = _enemyBullets.length - 1; i >= 0; i--) 
			{
				b = _enemyBullets[i];
			 
				b.x +=  Math.cos(b.rotation + 1.5) * Enum.enemyNormalBulletSpeed;
				// On Y axis use the sinus angle
				b.y +=  Math.sin(b.rotation + 1.5) * Enum.enemyNormalBulletSpeed;
				
				if (b.y > 800 || b.y <= 0) 
				{
					destroyEnemyBullet(b);
				}
			}
		}
		
		public function fire(enemy:EnemyNormal):void 
		{
			var b:EnemyBullet = pool.getSprite() as EnemyBullet;
			b.enemy = enemy;
			play.addChild(b);
			b.x = enemy.x ;
			b.y = enemy.y ;
			b.rotation = enemy.rotation;
			_enemyBullets.push(b);
			
			Assets.shoot.play();
		}
		
	 
		
		public function destroyEnemyBullet(b:EnemyBullet):void 
		{
			var len:int = _enemyBullets.length;
			
			for (var i:int = 0; i < len; i++) 
			{
				if (_enemyBullets[i] == b) 
				{
					_enemyBullets.splice(i, 1);
					b.removeFromParent(true);
					pool.returnSprite(b);
				}
			}
		}
		
		private function destroyBullet(b:EnemyBullet):void 
		{
			var len:int = _enemyBullets.length;
			
			for (var i:int = 0; i < len; i++) 
			{
				if (_enemyBullets[i] == b) 
				{
					_enemyBullets.splice(i, 1);
					b.removeFromParent(true);
					pool.returnSprite(b);
				}
			}
		}
		
		public function desctroy():void
		{
			pool.destroy();
			pool = null;
			_enemyBullets = null;
		}
		
		public function get enemyBullets():Array 
		{
			return _enemyBullets;
		}
		
		public function set enemyBullets(value:Array):void 
		{
			_enemyBullets = value;
		}
	}

}