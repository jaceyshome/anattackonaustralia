package controls 
{
	import core.Assets;
	import core.Enum;
	import flash.events.MouseEvent;
	import vos.EnemyNormal;
	import starling.core.Starling;
	import states.Play;
	
	public class EnemyNormalManager 
	{
		private var play:Play;
		private var _enemies:Array;
		private var pool:StarlingPool;
		private var count:int = 0;
		private var _enemyNormalBulletManager:EnemyNormalBulletManager;
		
		public function EnemyNormalManager(play:Play) 
		{
			this.play = play;
			_enemies = new Array();
			pool = new StarlingPool(EnemyNormal, Enum.enemyNormalMaxNumber);
			_enemyNormalBulletManager = new EnemyNormalBulletManager(this.play);
		}
		
	 
		public function update():void 
		{
			if (Math.random() < Enum.enemyNormalWait && _enemies.length <= Enum.enemyWaveMax + play.getScore()/1000)
			//if (Math.random() < Enum.enemyNormalWait && _enemies.length < 1)
			{
				spawn();	
			}
			
			var enemy:EnemyNormal;
			for (var i:int = _enemies.length - 1; i >= 0; i--) 
			{
				enemy = _enemies[i] ;
				if(enemy.y <= enemy.stopPositionY)
				{
					enemy.y += 8;
				}
				else {
					
					if(count % 2 == 0)
					{
						var cx:Number = play.hero.x - enemy.x;
						var cy:Number = play.hero.y - enemy.y;
						var radians:Number = Math.atan2(cy, cx) ;
						//trace("radians: ", radians);
						enemy.rotation = radians - 1.5;
						
						if (count % 30 == 0)
						{
							_enemyNormalBulletManager.fire(enemy);
						}
					}
					
				}
				
				if (enemy.y > Enum.stageHeight) 
				{
					destroyEnemyNormal(enemy);
				}			
			}
			
			_enemyNormalBulletManager.update();
			count++;
		}
		
		private function spawn():void 
		{
			var enemy:EnemyNormal = pool.getSprite() as EnemyNormal;
			enemy.y = -50;
			enemy.x = Math.random() * 700 + 50;
			
			play.addChild(enemy);
			_enemies.push(enemy);
		}
		
		public function destroyEnemyNormal(enemy:EnemyNormal):void 
		{
			var len:int = _enemies.length;
			
			for (var i:int = 0; i < len; i++) 
			{
				if (_enemies[i] == enemy) 
				{
					_enemies.splice(i, 1);
					enemy.removeFromParent(true);
					pool.returnSprite(enemy);
				}
			}
		}
		
		public function desctroy():void
		{
			_enemyNormalBulletManager.desctroy();
			pool.destroy();
			pool = null;
			count = 0;
			_enemies = null;
		}
		
		public function get enemies():Array 
		{
			return _enemies;
		}
		
		public function set enemies(value:Array):void 
		{
			_enemies = value;
		}
		
		public function get enemyNormalBulletManager():EnemyNormalBulletManager 
		{
			return _enemyNormalBulletManager;
		}
		
		public function set enemyNormalBulletManager(value:EnemyNormalBulletManager):void 
		{
			_enemyNormalBulletManager = value;
		}
	}

}