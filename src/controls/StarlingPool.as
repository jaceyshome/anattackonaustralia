package controls 
{
	import starling.display.DisplayObject;
	
	
	public class StarlingPool 
	{
		private var counter:int;
		private var _items:Array;
		
		public function StarlingPool(type:Class, len:int) 
		{
			_items = new Array();
			counter = len;
			
			var i:int = len;
			while (--i > -1) 
			{
				_items[i] = new type();
			}
		}
		
		public function getSprite():DisplayObject 
		{
			if (counter > 0) 
			{
				return _items[--counter];
			}
			else 
			{
				throw new Error("You exhausted the pool!");
			}
		}
		
		public function returnSprite(s:DisplayObject):void 
		{
			_items[counter++] = s;
		}
		
		public function destroy():void 
		{
			_items = null; 
		}
		
		public function get items():Array 
		{
			return _items;
		}
		
		public function set items(value:Array):void 
		{
			_items = value;
		}
	}

}