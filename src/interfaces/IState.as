package interfaces
{
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public interface IState 
	{
		function update():void;	
		function destroy():void; 
	}
	
}