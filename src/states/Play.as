package states
{
	import core.Enum;
	import core.Game;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import interfaces.*;
	import controls.*;
	import vos.*;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Play  extends Sprite implements IState
	{
		private var _game:Game;
		private var background:Background;
		private var _cloudManager:CloudManager;
		private var _hero:Hero;
		private var _bulletManager:BulletManager;
		private var _fire:Boolean = false;
		private var ns:Stage;
		private var _enemyNormalManager:EnemyNormalManager;
		private var _collisionManager:CollisionManager;
		private var _explosionManager:ExplosionManager;
		private var _score:Score;
		private var _heroLife:HeroLife;
		
		public function Play(game:Game)
		{
			this._game = game;
			this.addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			background = new Background();
			addChild(background);
			
			_enemyNormalManager = new EnemyNormalManager(this);
			_cloudManager = new CloudManager(this);
			_bulletManager = new BulletManager(this);
			
			_collisionManager = new CollisionManager(this);
			_explosionManager = new ExplosionManager(this);
			
			_hero = new Hero(this);
			_hero.x = Enum.stageWidth / 2;
			_hero.y = Enum.stageHeight;
			addChild(_hero);
			
			_score = new Score();
			addChild(score);
			score.x = 450;
			
			_heroLife = new HeroLife();
			addChild(_heroLife);
			//_heroLife.x = 20;
			_heroLife.y = Enum.stageHeight - 100;
			_heroLife.updateLife(Enum.heroLife);
			
			ns = Starling.current.nativeStage;
			ns.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			ns.addEventListener(MouseEvent.MOUSE_UP, onUp);
		}
		
		private function onDown(event:MouseEvent):void
		{ 
			_fire = true;
		}
		
		private function onUp(event:MouseEvent):void
		{
			_fire = false;
			_bulletManager.resetBulletCounter();
		}
		
		public function update():void
		{
			background.update();
			_cloudManager.update();
			
			_hero.update();
			_bulletManager.update();
			_enemyNormalManager.update();
			_collisionManager.update();
		}
		
		public function destroy():void
		{
			ns.removeEventListener(MouseEvent.MOUSE_DOWN, onDown);
			ns.removeEventListener(MouseEvent.MOUSE_UP, onUp);
			
			_bulletManager.desctroy();
			_cloudManager.desctroy();
			
			_enemyNormalManager.desctroy();
			removeFromParent(true);
		}
		
		public function updateHeroLife(value:int):void 
		{
			_heroLife.updateLife(value);
		}
		
		
		public function  addScore(value:int):void 
		{
			_score.addScore(value);
		}
		
		
		public function  getScore():int 
		{
			return _score.getScore();
		}
		
		
		
		
		
		
		
		public function get hero():Hero 
		{
			return _hero;
		}
		
		public function set hero(value:Hero):void 
		{
			_hero = value;
		}
		
		public function get bulletManager():BulletManager 
		{
			return _bulletManager;
		}
		
		public function set bulletManager(value:BulletManager):void 
		{
			_bulletManager = value;
		}
		
		public function get enemyNormalManager():EnemyNormalManager 
		{
			return _enemyNormalManager;
		}
		
		public function set enemyNormalManager(value:EnemyNormalManager):void 
		{
			_enemyNormalManager = value;
		}
		
		public function get game():Game 
		{
			return _game;
		}
		
		public function set game(value:Game):void 
		{
			_game = value;
		}
		
		public function get fire():Boolean 
		{
			return _fire;
		}
		
		public function set fire(value:Boolean):void 
		{
			_fire = value;
		}
		
		public function get explosionManager():ExplosionManager 
		{
			return _explosionManager;
		}
		
		public function set explosionManager(value:ExplosionManager):void 
		{
			_explosionManager = value;
		}
		
		public function get score():Score 
		{
			return _score;
		}
		
		public function set score(value:Score):void 
		{
			_score = value;
		}
	
	}

}