package states
{
	import core.Assets;
	import core.Game;
	import interfaces.IState;
	import vos.Background;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class Menu  extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
		private var play:Button;
		private var logoText:TextField;
		
		public function Menu(game:Game)
		{
			this.game = game;
			this.addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			background = new Background();
			addChild(background);
			
			//name logo is from atlas.xml
			logoText = new TextField(800, 200, "An Attack on Australia", "logoFont", 72, 0xFFFFFF);
			logoText.hAlign = "center";
			logoText.y = 200;
			addChild(logoText);
			
			play = new Button(Assets.ta.getTexture("playButton"));
			play.addEventListener(Event.TRIGGERED, onPlay);
			play.pivotX = play.width * 0.5;
			play.x = 400;
			play.y = 450;
			addChild(play);
		}
		
		private function onPlay(e:Event):void 
		{
			//TODO Auto Generated method stub
			game.changeState(Game.PLAY_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			//dispose to true will remove everything: including eventListener
			background.removeFromParent(true);
			background = null;
			play.removeFromParent(true);
			play = null;
			this.removeFromParent(true);
		}
	
	}

}