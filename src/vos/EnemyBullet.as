package vos 
{
	import core.Assets;
	import starling.display.Sprite;
	import starling.display.Image;

	public class EnemyBullet extends Sprite 
	{
		public var enemy:EnemyNormal;
		
		public function EnemyBullet() 
		{
			var img:Image = new Image(Assets.ta.getTexture("enemyBullet"));
			img.pivotX = img.width * 0.5;
			img.pivotY = img.height * 0.5;
			addChild(img);	
		}
		
	}

}