package vos 
{
	import core.Assets;
	import core.Enum;
	import starling.display.Sprite;
	import starling.display.Image;

	
	public class EnemyNormal extends Sprite 
	{	
		public var stopPositionY:Number = 0;
		private var life:int = 100;
		
		public function EnemyNormal() 
		{	
			var img:Image = new Image(Assets.ta.getTexture("enemy"));
			stopPositionY =  (Math.ceil(Math.random() * 5)) * img.height + img.height;
			img.pivotX = img.width  / 2.0;
			img.pivotY = img.height / 2.0;		
			addChild(img);	
			
			life = 100;
			
		}
		
		public function getLife():int 
		{
			return life;
		}
		
		public function getAttack():void 
		{
			life -= 10;
		}
		
		public function  resetLife():void 
		{
			life = 100;
			this.rotation = 0;
		}
	}

}