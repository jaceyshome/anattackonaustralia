package vos 
{
	import core.Assets;
	import core.Enum;
	import starling.display.Sprite;
	import starling.display.Image;

	public class Cloud extends Sprite 
	{
		public var speed:int = 0;
		
		public function Cloud() 
		{
			var index:int = Math.ceil(Math.random() * 3);
			var imgName:String = "cloud" + index.toString();
			var img:Image = new Image(Assets.ta.getTexture(imgName));
			rotation = Math.ceil(Math.random() * 4) / 10;
			pivotX = width * 0.5;
			pivotY = height * 0.5;
			speed = Math.ceil(Math.random() * Enum.cloudMaxSpeed) + Enum.cloudMinSpeed;
			addChild(img);	
		}
		
	}

}