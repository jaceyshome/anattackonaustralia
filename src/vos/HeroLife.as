package vos 
{
	import core.Assets;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;

	public class HeroLife extends Sprite 
	{
		private var life:TextField;
		
		public function HeroLife() 
		{
			var img:Image = new Image(Assets.ta.getTexture("heart"));
			addChild(img);	
			img.y = 40;
			img.x = 20;
			
			life = new TextField(300, 100, "0", "KomikaAxis", 32, 0xFFFFFF);
			life.hAlign = "left";
			life.x = 60;
			life.y = 0;
			addChild(life);
		}
		
		public function updateLife(value:Number):void 
		{
			life.text = value.toString();
		}
	}

}