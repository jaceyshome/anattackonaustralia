package vos 
{
	import core.Assets;
	import core.Enum;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class Background extends Sprite 
	{
		private var bg1:Image;
		private var bg2:Image;
		private var bg3:Image;
		 
		public function Background() 
		{
			bg1 = new Image(Assets.bgTexture1);
			bg2 = new Image(Assets.bgTexture2);
			bg3 = new Image(Assets.bgTexture3);
			 
			bg1.blendMode = BlendMode.NONE;
			bg2.blendMode = BlendMode.NONE;
			bg3.blendMode = BlendMode.NONE;
			
			bg2.y = -2048;
			bg3.y = -4096;
			
			addChild(bg1);
			addChild(bg2);
			addChild(bg3);
		}
		
		public function update():void 
		{
			bg1.y += Enum.bgSpeed;
			bg2.y += Enum.bgSpeed;
			bg3.y += Enum.bgSpeed;
		}
	}

}