package vos 
{
	import core.Assets;
	import core.Enum;
	import flash.events.MouseEvent;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.extensions.PDParticleSystem;
	import states.Play;
	
	public class Hero extends Sprite 
	{
		private var play:Play;
		private var jetSmokeR:PDParticleSystem;
		private var jetSmokeL:PDParticleSystem;
		private var life:int = 0;
		
		public function Hero(play:Play) 
		{
			this.play = play;
		
			var img:Image = new Image(Assets.ta.getTexture("hero"));
			img.pivotX = img.width * 0.5;
			img.pivotY = img.height * 0.5;
			addChild(img);
			
			life = 100;
			
			
			//particle
			jetSmokeR = new PDParticleSystem(XML(new Assets.heroSmokeXML()),
				Assets.ta.getTexture("heroSmoke"));
			Starling.juggler.add(jetSmokeR);
			play.addChild(jetSmokeR);
			
			jetSmokeL = new PDParticleSystem(XML(new Assets.heroSmokeXML()),
				Assets.ta.getTexture("heroSmoke"));
			Starling.juggler.add(jetSmokeL);
			play.addChild(jetSmokeL);
			
			jetSmokeR.start();
			jetSmokeL.start();
		}
		
		public function update():void 
		{
			this.x += (Starling.current.nativeStage.mouseX - this.x) * 0.3;
			this.y += (Starling.current.nativeStage.mouseY - this.y) * 0.3;
			
			//jetSmoke.visible = (jetSmoke.emitterX != x);
			
				jetSmokeL.emitterX = this.x - 14;
				jetSmokeL.emitterY = this.y + 12;
				jetSmokeR.emitterX = this.x + 14;
				jetSmokeR.emitterY = this.y + 12;
				
		 	
		}
		
		public function getAttack():int 
		{
			this.life -= 10;
			return this.life;
		}
		
		public function getLife():int 
		{
			return this.life;
		}
	}

}