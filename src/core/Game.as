package core
{
	import interfaces.IState;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.VAlign;
	import starling.utils.HAlign;
	import states.GameOver;
	import states.Menu;
	import states.Play;

	public class Game extends Sprite
	{
		public static const MENU_STATE:int = 0;
		public static const PLAY_STATE:int = 1;
		public static const GAMEOVER_STATE:int = 2;
		
		private var current_state:IState;
		
		public function Game()
		{
			Assets.init();
			addEventListener(Event.ADDED_TO_STAGE, init);
		}

		protected function init(event:Event):void
		{
			//go to menu state when game start
			changeState(MENU_STATE);
			this.addEventListener(Event.ENTER_FRAME, update);
		}
		
		public function changeState(state:int):void
		{
			if (current_state != null)
			{
				//destroy the current state before changing state
				current_state.destroy();
				current_state = null;
			}
			
			switch (state) 
			{
				case MENU_STATE:
					current_state = new Menu(this);
					break;
				case PLAY_STATE:
					current_state = new Play(this);
					break;
				case GAMEOVER_STATE:
					current_state = new GameOver(this);
					break;
			}
			
			addChild(Sprite(current_state));
		}
		
		private function update(e:Event):void 
		{
			current_state.update();
		}

	 
	}

}