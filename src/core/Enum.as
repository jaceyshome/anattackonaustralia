package core 
{
	public class Enum 
	{
		public static const stageHeight:int = 800;
		public static const stageWidth:int = 800;
		
		public static const bgSpeed:Number = 0.2;
		
		public static const cloudMaxSpeed:Number = 7;
		public static const cloudMinSpeed:Number = 2;
		public static const cloudSpawnWait:Number = 0.02;
		public static const cloudTotalNumber:int = 30;
		
		public static const enemyNormalMaxNumber:int = 30;
		public static const enemyWaveMax:int = 5;
		public static const enemyNormalWait:Number = 0.05;
		public static const enemyNormalBulletSpeed:Number = 8;
		public static const enemyNormalBulletMaxNumber:int = 1000;
		
		public static const heroLife:int = 100;
		
	}

}