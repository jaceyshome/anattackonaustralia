package core 
{
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	public class Assets 
	{
		[Embed(source="../../assets/background1.jpg")]
		private static var bg1:Class;
		public static var bgTexture1:Texture;
		
		[Embed(source="../../assets/background2.jpg")]
		private static var bg2:Class;
		public static var bgTexture2:Texture;
		
		[Embed(source="../../assets/background3.jpg")]
		private static var bg3:Class;
		public static var bgTexture3:Texture;
		
		[Embed(source = "../../assets/atlas.png")]
		private static var atlas:Class;
		
		public static var ta:TextureAtlas;
		
		[Embed(source = "../../assets/atlas.xml", mimeType="application/octet-stream")]
		private static var atlasXML:Class;
	
		//bigmap font
		[Embed(source = "../../assets/komika.png")]
		private static var komika:Class;
		
		[Embed(source = "../../assets/komika.fnt", mimeType="application/octet-stream")]
		private static var komikaXML:Class;
		
		
		//particle 
		[Embed(source = "../../assets/heroSmoke.pex", mimeType="application/octet-stream")]
		public static var heroSmokeXML:Class;
		
		[Embed(source = "../../assets/enemyNormalSmoke.pex", mimeType="application/octet-stream")]
		public static var enemyNormalSmokeXML:Class;
		
		//[Embed(source = "../../assets/smoke.pex", mimeType="application/octet-stream")]
		//public static var smokeXML:Class;
		
		[Embed(source = "../../assets/explosion.pex", mimeType="application/octet-stream")]
		public static var explosionXML:Class;
		
		
		//sound
		[Embed(source = "../../assets/explosion.mp3")]
		private static var explosionSound:Class;
		public static var explosion:Sound;
		
		[Embed(source = "../../assets/shoot.mp3")]
		private static var shootSound:Class;
		public static var shoot:Sound;
		
		public static function init(): void
		{
			bgTexture1 = Texture.fromBitmap(new bg1());
			bgTexture2 = Texture.fromBitmap(new bg2());
			bgTexture3 = Texture.fromBitmap(new bg3());
			
			ta = new TextureAtlas(Texture.fromBitmap(new atlas()),
				XML(new atlasXML()));
				
			TextField.registerBitmapFont(new BitmapFont(Texture.fromBitmap(new komika()),
				XML(new komikaXML())));
				
			explosion = new explosionSound();
			//flash player bug: load the sound when need to play it and it has delpay
			//so play it once when game start and with volume set to 0 to load it into menory
			explosion.play(0, 0, new SoundTransform(0));
			
			shoot = new shootSound();
			shoot.play(0, 0, new SoundTransform(0));
		}
		
		
	}

}